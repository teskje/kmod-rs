#![no_std]

use linux::ffi::c_int;
use linux::println;

#[no_mangle]
pub fn init_module() -> c_int {
    println!("module init");
    0
}

#[no_mangle]
pub fn cleanup_module() {
    println!("module cleanup");
}
