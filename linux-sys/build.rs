use std::env;
use std::error::Error;
use std::path::PathBuf;
use std::process::Command;

use shlex::Shlex;

const WRAPPER_HEADER: &str = "src/linux.h";
const KDIR_ENV: &str = "KDIR";

/// Create bindings only for items specified here.
const FUNCTION_WHITELIST: &[&str] = &["__kmalloc", "kfree", "printk"];
const TYPE_WHITELIST: &[&str] = &[];
const VAR_WHITELIST: &[&str] = &["EX_GFP_KERNEL"];

fn main() {
    // Only rerun if we build against headers.
    println!("cargo:rerun-if-changed={}", WRAPPER_HEADER);
    println!("cargo:rerun-if-env-changed={}", KDIR_ENV);

    let kdir = get_kdir().expect("Error getting kdir");
    let cflags = make_cflags(&kdir).expect("Error generating cflags");

    let mut builder = bindgen::builder()
        .use_core()
        .ctypes_prefix("cty")
        .header(abs_path(WRAPPER_HEADER))
        .clang_args(cflags);

    for func in FUNCTION_WHITELIST {
        builder = builder.whitelist_function(func);
    }
    for typ in TYPE_WHITELIST {
        builder = builder.whitelist_type(typ);
    }
    for var in VAR_WHITELIST {
        builder = builder.whitelist_var(var);
    }

    let bindings = builder.generate().expect("Error generating linux bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Error writing linux bindings");
}

fn get_kdir() -> Result<String, Box<Error>> {
    let kdir = if let Ok(kdir) = env::var(KDIR_ENV) {
        kdir
    } else {
        // Default to "/lib/modules/$(uname -r)/build".
        let name = get_output(Command::new("uname").arg("-r"))?;
        format!("/lib/modules/{}/build", name)
    };

    Ok(kdir)
}

fn make_cflags(kdir: &str) -> Result<Vec<String>, Box<Error>> {
    let cflags_path = abs_path("cflags");

    let mut make = Command::new("make");
    make.args(&["-C", kdir])
        .arg(format!("M={}", cflags_path))
        .arg("CC=clang");

    let cflags_str = get_output(&mut make)?;
    make.arg("clean").output()?;

    let mut cflags: Vec<_> = Shlex::new(&cflags_str).collect();

    // `cflags` may contain paths relative to `kdir`, so we need to
    // change Clangs working directory too.
    cflags.push(format!("-working-directory={}", kdir));

    Ok(cflags)
}

fn abs_path(rel_path: &str) -> String {
    let mut path = env::current_dir().unwrap();
    path.push(rel_path);
    path.to_str().unwrap().into()
}

fn get_output(cmd: &mut Command) -> Result<String, Box<Error>> {
    let output = cmd.output()?.stdout;
    let output = String::from_utf8(output)?;
    Ok(output.trim().into())
}
