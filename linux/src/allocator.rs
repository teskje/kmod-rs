use crate::alloc::alloc::{GlobalAlloc, Layout};
use crate::ffi::c_void;

pub struct LinuxAllocator;

unsafe impl GlobalAlloc for LinuxAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        linux_sys::kmalloc(layout.size(), linux_sys::GFP_KERNEL) as *mut u8
    }

    unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
        linux_sys::kfree(ptr as *const c_void);
    }
}

#[alloc_error_handler]
pub fn alloc_error(_layout: Layout) -> ! {
    panic!("alloc error");
}
