# kmod-rs

A Linux kernel module written (mostly) in Rust.

## Requirements

### Linux Kernel Headers

Install the linux-header package provided by your distribution's package manager, e.g.:

- Ubuntu:

      # apt install linux-headers-generic

- Arch Linux:

      # pacman -S linux-headers

Afterwards the kernel headers should be available under ``/lib/modules/$(uname -r)/build``.

### cargo-xbuild

Building the kernel module requires Rust nightly and [cargo-xbuild](https://github.com/rust-osdev/cargo-xbuild):

    $ rustup install nightly
    $ rustup component add rust-src --toolchain=nightly
    $ cargo +nightly install cargo-xbuild

### Clang

To generate kernel bindings, Clang needs to be installed. For instructions see the [rust-bindgen documentation](https://rust-lang-nursery.github.io/rust-bindgen/requirements.html).


## Usage

Once all above requirements are installed, build the example kernel module:

    $ cd hello-kmod
    $ make

This will produce the file ``hello.ko`` in the same directory, which can then be loaded into a running Linux kernel:

    # insmod hello.ko
    # rmmod hello.ko
